import React, {Component} from 'react';
import withQueryParams from 'react-router-query-params';

@withQueryParams()
class SortTh extends Component {
    render() {
        return (
            <th className={this.getClassName()} onClick={this.setSort}>{this.props.children}</th>
        );
    }

    getClassName() {
        if (this.props.queryParams.sort === this.props.sort) {
            return this.props.queryParams.order;
        }
        return '';
    }

    setSort = () => {
        const qp = this.props.queryParams;
        const sort = this.props.sort;
        const order = (qp.sort === sort && qp.order === 'asc') ? 'desc' : 'asc';
        this.props.setQueryParams({sort, order});
    };
}

export default SortTh;
