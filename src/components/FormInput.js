import React, {Component} from 'react';
import {observer} from 'mobx-react';
import * as cn from 'classnames';

@observer
class FormInput extends Component {
    render() {
        return (
            <div className="form-group row">
                <label className="col-form-label col-lg-4">{this.props.label}</label>
                <div className="col-lg-8">
                    <input
                        className={this.classes}
                        type={this.props.type || 'text'}
                        name={this.props.name}
                        value={this.value}
                        required={!this.props.notRequired}
                        pattern={this.props.pattern}
                        maxLength="50"
                        onChange={this.props.onChange}
                        onBlur={this.props.onBlur}
                    />
                    {this.error && <span className="form-text text-danger">{this.error}</span>}
                </div>
            </div>
        );
    }

    get value() {
        return this.props.form.fd[this.props.name];
    }

    get error() {
        return this.props.form.err[this.props.name];
    }

    get classes() {
        return cn({
            'form-control': true,
            'text-danger' : this.error,
            'border-success' : (!this.error && this.value)
        });
    }
}

export default FormInput;
