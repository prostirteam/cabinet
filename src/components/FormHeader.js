import React, {Component} from 'react';

class FormHeader extends Component {
    render() {
        return (
            <div className="card-header header-elements-inline">
                <h5 className="card-title">{this.props.children}</h5>
                <div className="header-elements">
                    <div className="list-icons">
                        <span className="list-icons-item" data-action="collapse"> </span>
                        <span className="list-icons-item" data-action="reload"> </span>
                    </div>
                </div>
            </div>
        );
    }
}

export default FormHeader;
