import React, {Component} from 'react';
import {Link, NavLink, withRouter} from 'react-router-dom';
import {inject} from 'mobx-react';
import {logout} from '../api';

@inject('authStore')
@withRouter
class Sidebar extends Component {
    render() {
        const user = this.props.authStore.user;

        return (
            <div className="sidebar sidebar-light sidebar-main sidebar-fixed sidebar-expand-md">
                <div className="sidebar-mobile-toggler text-center">
                    <span className="sidebar-mobile-main-toggle"><i className="icon-arrow-left8"/></span>
                    Navigation
                    <span className="sidebar-mobile-expand">
                        <i className="icon-screen-full"/>
                        <i className="icon-screen-normal"/>
                    </span>
                </div>
                <div className="sidebar-content">
                    <div className="sidebar-user-material">
                        <div className="sidebar-user-material-body">
                            <div className="card-body text-center">
                                <img src={user.avatar || '/img/avatar.png'}
                                     className="img-fluid rounded-circle shadow-1 mb-3 pr-avatar" alt="" />
                                <h6 className="mb-0 text-white text-shadow-dark">{user.name}</h6>
                                <span className="font-size-sm text-white text-shadow-dark">{user.email}</span>
                            </div>
                            <div className="sidebar-user-material-footer">
                                <a href="#user-nav"
                                   className="d-flex justify-content-between align-items-center text-shadow-dark dropdown-toggle"
                                   data-toggle="collapse">
                                    <span>Мій аккаунт</span>
                                </a>
                            </div>
                        </div>
                        <div className="collapse" id="user-nav">
                            <ul className="nav nav-sidebar">
                                <li className="nav-item">
                                    <Link to={'/dashboard/users/form/' + user.id} className="nav-link">
                                        <i className="icon-user-plus"/><span>Мій профіль</span>
                                    </Link>
                                </li>
                                <li className="nav-item" onClick={() => logout()}>
                                    <span className="nav-link"><i className="icon-switch2"/><span>Вийти</span></span>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div className="card card-sidebar-mobile">
                        <ul className="nav nav-sidebar" data-nav-type="accordion">
                            <li className="nav-item">
                                <NavLink exact to="/dashboard" className="nav-link" activeClassName="active">
                                    <i className="icon-home4"/><span>Головна</span>
                                </NavLink>
                            </li>
                            <li className="nav-item">
                                <NavLink to="/dashboard/manuals" className="nav-link" activeClassName="active">
                                    <i className="icon-briefcase"/><span>Довідники</span>
                                </NavLink>
                            </li>
                            <li className="nav-item">
                                <NavLink to="/dashboard/documents" className="nav-link" activeClassName="active">
                                    <i className="icon-file-text2"/><span>Документи</span>
                                </NavLink>
                            </li>
                            <li className="nav-item">
                                <NavLink to="/dashboard/forms" className="nav-link" activeClassName="active">
                                    <i className="icon-server"/><span>Форми</span>
                                </NavLink>
                            </li>
                            <li className="nav-item">
                                <NavLink to="/dashboard/reports" className="nav-link">
                                    <i className="icon-file-text2"/><span>Звіти</span>
                                </NavLink>
                            </li>
                            <li className="nav-item">
                                <NavLink to="/dashboard/mail" className="nav-link">
                                    <i className="icon-envelop2"/><span>Пошта</span>
                                </NavLink>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        );
    }
}

export default Sidebar;
