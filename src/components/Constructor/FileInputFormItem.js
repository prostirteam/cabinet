import React, {Component} from 'react';
import {action} from 'mobx';
import {observer} from 'mobx-react';
import {MIME_TYPES} from '../../helpers';
import {fileUpload} from '../../api';

@observer
class FileInputFormItem extends Component {
    field = this.props.field;

    render() {
        return (
            <div>
                <label className="mb-1">{this.field.label}</label>
                <div className="uniform-uploader">
                    <input
                        type="file" className="form-input-styled"
                        accept={MIME_TYPES} required={this.field.isRequired}
                        data-mimetypes={this.field.mimetypes} data-max-size={this.field.maxSize}
                        onChange={this.onChange}
                    />
                    <span className="action btn btn-sm bg-pink-400 legitRipple">Вибрати файл</span>
                </div>
                <div className="pt-2">
                    <a href={this.field.value} target="_blank" rel="noopener noreferrer">{this.field.value}</a>
                </div>
            </div>
        );
    }

    @action
    onChange = e => {
        const file = e.target.files[0];
        fileUpload(file).then(action(res => this.field.value = res.file));
    }
}

export default FileInputFormItem;
