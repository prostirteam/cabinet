import React, {Component} from 'react';
import {action} from 'mobx';
import {observer} from 'mobx-react';

@observer
class PreparedListFormItem extends Component {
    field = this.props.field;

    componentDidMount() {
        // window.PreparedListFormItem = this;
    }

    render() {
        return (
            <div className="">
                <div>{this.field.label}</div>
                <div>{this.field.data.name}</div>
                <div className="row">
                    <div className="col-md-3">
                        <select
                            className="form-control" required={this.field.isRequired}
                            value={this.field.value[0]}
                            onChange={e => this.onChange(e, 0)}
                        >
                            <option value=""/>
                            {this.firstLevel.map((item, i) => (
                                <option key={i} value={i}>{item.name}</option>
                            ))}
                        </select>
                    </div>
                    {
                        !!this.secondLevel.length &&
                        <div className="col-md-3">
                            <select
                                className="form-control" required={this.field.isRequired}
                                value={this.field.value[1]}
                                onChange={e => this.onChange(e, 1)}
                            >
                                <option value=""/>
                                {this.secondLevel.map((item, i) => (
                                    <option key={i} value={i}>{item.name}</option>
                                ))}
                            </select>
                        </div>
                    }
                    {
                        !!this.thirdLevel.length &&
                        <div className="col-md-3">
                            <select
                                className="form-control" required={this.field.isRequired}
                                value={this.field.value[2]}
                                onChange={e => this.onChange(e, 2)}
                            >
                                <option value=""/>
                                {this.thirdLevel.map((item, i) => (
                                    <option key={i} value={i}>{item.name}</option>
                                ))}
                            </select>
                        </div>
                    }
                    {
                        !!this.fourthLevel.length &&
                        <div className="col-md-3">
                            <select
                                className="form-control" required={this.field.isRequired}
                                value={this.field.value[3]}
                                onChange={e => this.onChange(e, 3)}
                            >
                                <option value=""/>
                                {this.fourthLevel.map((item, i) => (
                                    <option key={i} value={i}>{item.name}</option>
                                ))}
                            </select>
                        </div>
                    }
                </div>
            </div>
        );
    }

    get firstLevel() {
        return this.field.data.data;
    }

    get secondLevel() {
        const a = this.firstLevel[+this.field.value[0]];
        return a ? a.categories : [];
    }

    get thirdLevel() {
        const a = this.secondLevel[+this.field.value[1]];
        return a ? a.categories : [];
    }

    get fourthLevel() {
        const a = this.thirdLevel[+this.field.value[2]];
        return a ? a.categories : [];
    }

    @action
    onChange(e, i) {
        this.field.value[i] = e.target.value || -1;

        for(let y = i + 1; y < 4; y++) this.field.value[y] = -1;

        this.field._value = [
            this.field.value[0] > -1 ? this.firstLevel[this.field.value[0]].name  : '',
            this.field.value[1] > -1 ? this.secondLevel[this.field.value[1]].name : '',
            this.field.value[2] > -1 ? this.thirdLevel[this.field.value[2]].name  : '',
            this.field.value[3] > -1 ? this.fourthLevel[this.field.value[3]].name : '',
        ];
    }
}

export default PreparedListFormItem;
