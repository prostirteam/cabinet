import React, {Component} from 'react';
import withQueryParams from 'react-router-query-params';
import * as cn from 'classnames';

@withQueryParams()
class Pagination extends Component {
    perPage = 20;

    get page() {
        return +this.props.queryParams.page;
    }

    get pages() {
        return Math.ceil(this.props.total/this.perPage);
    }

    render() {
        if(this.pages < 20) return null;

        const p = Array(this.pages).fill(null).map((item, i) => i + 1);
        let pp = [];

        if(this.page < 10) {
            pp = p.slice(0, 10);

        } else if(this.page > this.pages - 10) {
            pp = p.slice(-10);

        } else {
            pp = p.slice(this.page - 6, this.page + 4);
        }

        return (
            <div className="pagination">
                {this.page > 1 &&
                    <div className="pagination-link prev" onClick={() => this.setPage(this.page - 1)}>&lt;&lt;</div>
                }
                {(this.pages > 10 && this.page >= 10) && <div className="pagination-link points">...</div>}
                {pp.map(n => (
                    <div className={cn({'pagination-link': true, 'active': n === this.page})}
                         key={n} onClick={() => this.setPage(n)}
                    >{n}</div>
                ))}
                {(this.pages > 10 && this.page < this.pages - 10) && <div className="pagination-link points">...</div>}
                {this.page < this.pages &&
                    <div className="pagination-link next" onClick={() => this.setPage(this.page + 1)}>&gt;&gt;</div>
                }
            </div>
        );
    }

    setPage = page => {
        if(page === this.page) return;
        this.props.setQueryParams({page});
    };
}

export default Pagination;
