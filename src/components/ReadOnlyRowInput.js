import React, {Component} from 'react';

class ReadOnlyRowInput extends Component {
    render() {
        return (
            <div className="form-group row">
                <label className="col-form-label col-lg-4">{this.props.label}</label>
                <div className="col-lg-8">
                    <input className="form-control" readOnly value={this.props.value}/>
                </div>
            </div>
        );
    }
}

export default ReadOnlyRowInput;
