import React, {Component} from 'react';
import Sidebar from '../components/Sidebar';
import Header from '../components/Header';
import Footer from '../components/Footer';
import {Route, Switch} from 'react-router-dom';
import Home from './Home';
import UserForm from './UserForm';
import DocumentList from './Document/DocumentList';
import ManualList from './Manual/ManualList';
import ManualItem from './Manual/ManualItem';
import FormList from './Forms/FormList';
import FormsHistoryList from './FormsHistoryList';
import MailPage from './MailPage';


class Dashboard extends Component {
    componentDidMount() {
        window._App.initBeforeLoad();
        window._App.initCore();
        window._App.initAfterLoad();
    }

    render() {
        return (
            <div id="dashboard" className="navbar-top">
                <Header />
                <div className="page-content">
                    <Sidebar />
                    <div className="content-wrapper">
                        <Switch>
                            <Route path='/dashboard'                  component={Home} exact      />
                            <Route path='/dashboard/users/form/:id'   component={UserForm}        />
                            <Route path='/dashboard/documents'        component={DocumentList}    />
                            <Route path='/dashboard/manuals-item/:id' component={ManualItem}      />
                            <Route path='/dashboard/manuals'          component={ManualList}      />
                            <Route path='/dashboard/forms'            component={FormList}        />
                            <Route path='/dashboard/reports'          component={FormsHistoryList}/>
                            <Route path='/dashboard/mail'             component={MailPage}        />
                        </Switch>
                        <Footer />
                    </div>
                </div>
            </div>
        );
    }
}

export default Dashboard;
