import React, {Component} from 'react';
import {action, observable} from 'mobx';
import {observer, inject} from 'mobx-react';
import {dateFormat, showMsg} from '../helpers';
import {ajax} from '../api';

@inject('authStore')
@observer
class MailPage extends Component {
    user = this.props.authStore.user;
    @observable loading = true;
    @observable messages = {count: "0", messages: [], users: {}};
    @observable msgText = '';

    componentDidMount() {
        // window.MailPage = this;
        this.fetchChatData();
    }

    fetchChatData() {
        ajax.get(`/emails`)
            .then(action(({status, data}) => {
                if(data) this.messages = data;
                this.loading = false;
            })).catch(e => showMsg('error', e.message));
    }

    render() {
        if(this.loading) return null;

        return (
            <div className="content">
                <div className="d-md-flex align-items-md-start">
                    <div className="flex-fill overflow-auto">
                        <div className="card">
                            <div className="card-header header-elements-inline">
                                <h5 className="card-title">Пошта</h5>
                                <div className="header-elements">
                                    <div className="list-icons">
                                        <span className="list-icons-item" data-action="collapse" />
                                        <span className="list-icons-item" data-action="reload" />
                                    </div>
                                </div>
                            </div>

                            <div className="card-body">
                                <ul className="media-list media-chat media-chat-scrollable mb-3">
                                    {
                                        this.messages.messages.reverse().map(item => (
                                            +this.user.id === +item.user_id ? (
                                                <li key={item.id} className="media media-chat-item-reverse">
                                                    <div className="media-body">
                                                        <div className="media-chat-item">{item.message}</div>
                                                        <div className="font-size-sm text-muted mt-2">
                                                            {dateFormat(item.create_time)}
                                                        </div>
                                                    </div>
                                                    <div className="ml-3">
                                                        <a href="/img/avatar.png">
                                                            <img
                                                                src="/img/avatar.png"
                                                                className="rounded-circle" width="40" height="40" alt=""
                                                            />
                                                        </a>
                                                    </div>
                                                </li>
                                            ) : (
                                                <li key={item.id} className="media">
                                                    <div className="mr-3">
                                                        <a href="/img/avatar.png">
                                                            <img
                                                                src="/img/avatar.png"
                                                                className="rounded-circle" width="40" height="40" alt=""
                                                            />
                                                        </a>
                                                    </div>
                                                    <div className="media-body">
                                                        <div className="media-chat-item">{item.message}</div>
                                                        <div className="font-size-sm text-muted mt-2">
                                                            {dateFormat(item.create_time)}
                                                        </div>
                                                    </div>
                                                </li>
                                            )
                                        ))
                                    }
                                    {!this.messages.messages.length &&
                                        <li className="text-center">Повідомнення відсутні..</li>
                                    }
                                </ul>

                                <form onSubmit={this.sendMessage} autoComplete="off">
                                    <textarea
                                        name="enter-message"
                                        className="form-control mb-3" rows="3"
                                        placeholder="Введіть Ваше повідомнення..."
                                        value={this.msgText}
                                        required
                                        maxLength="750"
                                        onChange={action(e => this.msgText = e.target.value)}
                                    />
                                    <div className="d-flex align-items-center">
                                        <div className="list-icons list-icons-extended">
                                        <span
                                            className="list-icons-item" data-popup="tooltip"
                                            data-container="body" title="" data-original-title="Send file"><i
                                            className="icon-file-plus"/></span>
                                        </div>
                                        <button
                                            className="btn bg-teal-400 btn-labeled btn-labeled-right ml-auto legitRipple">
                                            <b><i className="icon-paperplane"/></b> ВІДПРАВИТИ
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    @action
    sendMessage = e => {
        e.preventDefault();

        let data = {
            type: 'B_TO_A',
            message: this.msgText
        };

        ajax.post('/emails', data)
            .then(action(({status, data}) => {
                console.log('sendMessage; received data', data);
                this.fetchChatData();
                this.msgText = '';
            })).catch(e => showMsg('error', e.message));
    };
}

export default MailPage;
