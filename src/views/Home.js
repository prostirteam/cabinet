import React, {Component} from 'react';

class Home extends Component {
    render() {
        return (
            <div className="content">
                <div className="card">
                    <div className="card-header header-elements-inline">
                        <h5 className="card-title">Вітаємо в системі ПРОСТІР!</h5>
                        <div className="header-elements">
                            <div className="list-icons">
                                <span className="list-icons-item" data-action="collapse"> </span>
                                <span className="list-icons-item" data-action="reload"> </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Home;
