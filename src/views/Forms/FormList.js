import React, {Component} from 'react';
import {action, observable} from 'mobx';
import {observer} from 'mobx-react';
import * as cn from 'classnames';
import ConstructedForm from './ConstructedForm';
import {getFormById, getFormCategories, getForms} from '../../api';

@observer
class FormList extends Component {
    @observable loading = true;
    @observable categories = [];
    @observable forms = [];
    @observable aCategoryId = null;
    @observable aForm = null;

    componentDidMount() {
        // window.FormList = this;
        this.fetchData();
    }

    fetchData() {
        getFormCategories().then(action(data => {
            if(data) this.categories = data;
            this.loading = false;
        }));
    }

    render() {
        if(this.loading) return <div className="content">Loading ...</div>;

        return (
            <div className="content">
                <div className="card">
                    <div className="card-header header-elements-inline">
                        <h5 className="card-title">Форми</h5>
                    </div>
                    <div className="row">
                        <div className="col-md-6">
                            <table className="table datatable-selection-single pr-choice-table">
                                <thead><tr><th>Категорія</th></tr></thead>
                                <tbody>
                                {
                                    this.categories.map((item) => (
                                        <tr
                                            key={item.id}
                                            className={cn({a: this.aCategoryId === item.id})}
                                            onClick={() => this.setCategory(item.id)}
                                        ><td>{item.category}</td></tr>
                                    ))
                                }
                                </tbody>
                            </table>
                        </div>
                        <div className="col-md-6">
                            <table className="table datatable-selection-single pr-choice-table">
                                <thead><tr><th>Назва форми</th><th>Код</th></tr></thead>
                                <tbody>
                                {
                                    this.forms.map((item) => (
                                        <tr
                                            key={item.id}
                                            onClick={() => this.openConstructedForm(item.id)}
                                        >
                                            <td>{item.name}</td><td>{item.code}</td>
                                        </tr>
                                    ))
                                }
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                {this.aForm &&
                    <ConstructedForm
                        model={this.aForm}
                        onClose={action(() => this.aForm = null)}
                    />
                }
            </div>
        );
    }

    @action
    setCategory(id) {
        this.forms = [];
        this.aCategoryId = id;
        getForms(id).then(action(data => {
            if(data) this.forms = data;
        }));
    }

    openConstructedForm(id) {
        getFormById(id).then(action(data => {
            this.aForm = data;
        }))
    }
}

export default FormList;
