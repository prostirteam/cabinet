import React, {Component} from 'react';
import {action} from 'mobx';
import {observer} from 'mobx-react';
import PreparedListFormItem from '../../components/Constructor/PreparedListFormItem';
import FileInputFormItem from '../../components/Constructor/FileInputFormItem';
import {showMsg} from '../../helpers';
import {sendConstructedForm} from '../../api';

@observer
class ConstructedForm extends Component {
    model = this.props.model;

    componentDidMount() {
        // window.ConstructedForm = this;
    }

    render() {
        return (
            <div id="constructor-preview-modal" className="modal fade show">
                <div className="modal-dialog pl-4 pr-4">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title">Форма: {this.model.name}, код: {this.model.code}</h5>
                            <button type="button" className="close" onClick={this.props.onClose}>×</button>
                        </div>
                        <div className="row p-4">
                            {this.getFields()}
                        </div>
                        <div className="text-right p-3">
                            <button className="btn btn-default legitRipple mr-2" onClick={this.props.onClose}
                            >Закрити</button>
                            {this.model.is_updated !== false &&
                                <button className="btn btn-primary legitRipple" onClick={() => this.submit()}
                                >Зберегти</button>
                            }
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    @action
    getFields() {
        const a = [];
        for(let k in this.model.fields) {
            const field = this.model.fields[k];
            field.id = k;
            a.push(this.getField(field));
        }

        return a;
    }

    getField(field) {
        let node = null;

        switch(field.type) {
            case 'textBlock':
                node = field.text;
                break;

            case 'attachedFile':
                node = <a href={field.url} download target="_blank" rel="noopener noreferrer">{field.text}</a>;
                break;

            case 'text':
                node = (
                    <div>
                        <label>{field.label}</label>
                        <input
                            type="text" className="form-control" name={field.name}
                            required={field.isRequired} pattern={field.dataType}
                            value={field.value}
                            onChange={action(e => { field.value = e.target.value; this.forceUpdate(); })}
                        />
                    </div>
                );
                break;

            case 'select':
                node = (
                    <div>
                        <label>{field.label}</label>
                        <select
                            className="form-control" name={field.name} required={field.isRequired}
                            value={field.value}
                            onChange={action(e => { field.value = e.target.value; this.forceUpdate(); })}
                        >
                            <option value=""/>
                            {field.values.map((item, i) => (
                                <option key={i} value={item}>{item}</option>
                            ))}
                        </select>
                    </div>
                );
                break;

            case 'prepared-list':
                node = <PreparedListFormItem field={field} />;
                break;

            case 'checkbox':
                node = (
                    <div className="form-check">
                        <label className="form-check-label">
                            <div className="uniform-checker">
                                <input
                                    type="checkbox" className="form-check-input-styled"
                                    name={field.name} required={field.isRequired} checked={field.value}
                                    onChange={action(e => { field.value = e.target.checked; this.forceUpdate(); })}
                                />
                                <span/>
                            </div>
                            {field.label}
                        </label>
                    </div>
                );
                break;

            case 'radiobox':
                node = (
                    <div>
                        <label className="mb-1">{field.label}</label>
                        <div>
                            {
                                field.values.map((item, i) => (
                                    <div key={i + item} className="form-check form-check-inline">
                                        <label className="form-check-label">
                                            <div className="uniform-choice">
                                                <span className={field.value === item ? 'checked' : ''}>
                                                    <input
                                                        type="radio" className="form-input-styled"
                                                        name={field.name} value={item}
                                                        checked={field.value === item}
                                                        onChange={action(e => { field.value = e.target.value; this.forceUpdate(); })}
                                                    />
                                                </span>
                                            </div>
                                            {item}
                                        </label>
                                    </div>
                                ))
                            }
                        </div>
                    </div>
                );
                break;

            case 'file':
                node = <FileInputFormItem field={field} />;
                break;

            default:
                console.log('wrong type');
                return null;
        }
        return (
            <div className={'pro-form-block form-group col-md-' + ((field.size || 100) / 100 * 12)} key={field.id}>
                {node}
            </div>
        );
    }

    submit() {
        const data = {form_id: this.model.id, fields: {}};
        if(this.model.history_id) data.history_id = this.model.history_id;

        for(let k in this.model.fields) {
            const field = this.model.fields[k];
            switch(field.type) {
                case 'textBlock':
                case 'attachedFile':
                    break;

                case 'text':
                case 'checkbox':
                case 'radiobox':
                case 'file':
                    data.fields[field.name] = field.value;
                    break;

                case 'select':
                    data.fields[field.name] = field.values[+field.value];
                    break;

                case 'prepared-list':
                    data.fields[field.name] = JSON.stringify(field._value);
                    break;

                default:
                    console.log('wrong type');
                    return null;
            }
        }

        sendConstructedForm(data)
            .then(res => {
                console.log('sendConstructedForm', res);
            })
            .catch(err => showMsg('error', err.message));

        this.props.onClose();
    }
}

export default ConstructedForm;
