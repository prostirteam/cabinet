import React, {Component} from "react";
import {action, observable} from 'mobx';
import {observer} from 'mobx-react';
import {passwordRecovery} from '../../api';

@observer
class PwdRecoveryPage extends Component {
    @observable fd = {email: ''};
    @observable err = this.makeErrObject();

    render() {
        return (
            <div className="page-content login-cover">
                <div className="content-wrapper">
                    <div className="content d-flex justify-content-center align-items-center">
                        <form className="login-form" onSubmit={this.onSubmit} autoComplete="off" noValidate>
                            <div className="card mb-0">
                                <div className="card-body">
                                    <div className="text-center mb-3">
                                        <i className="icon-spinner11 icon-2x text-warning border-warning border-3 rounded-round p-3 mb-3 mt-1" />
                                        <h5 className="mb-0">Відновлення паролю</h5>
                                        <span className="d-block text-muted pl-1 pr-1">Ми надішлемо Вам інструкції по електронній пошті</span>
                                    </div>

                                    <div className="form-group form-group-feedback form-group-feedback-right">
                                        <input
                                            type="email" className="form-control" placeholder="Ваш e-mail"
                                            name="email" value={this.fd.email} required
                                            onChange={this.onInputChange}
                                            onBlur={this.validate}
                                        />
                                        {this.err.email && <span className="form-text text-danger">{this.err.email}</span>}
                                        <div className="form-control-feedback"><i className="icon-mail5 text-muted"/></div>
                                    </div>

                                    <button type="submit" className="btn bg-blue btn-block legitRipple">
                                        <i className="icon-spinner11 mr-2" />
                                        Скинути пароль
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        );
    }

    submit() {
        passwordRecovery(this.fd).then(action(res => {
            if(res.errors) this.setErrors(res.errors);
            else this.props.history.push('/success-recovery');
        }))
    };
}

export default PwdRecoveryPage;
