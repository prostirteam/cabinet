import React from 'react';
import {action, observable} from 'mobx';
import {observer} from 'mobx-react';
import {Link} from 'react-router-dom';
import FormComponent from '../../abstract/FormComponent';
import {registration} from '../../api';

@observer
class Registration extends FormComponent {
    @observable
    fd = {
        user_email: '',
        user_phone: '',
        user_name: '',
        user_position: '',
        bank_user_type: '',
        user_password: '',

        bank_name: '',
        bank_address: '',
        bank_phones: '',
        bank_emails: '',
        bank_ihe: '',
        bank_certificate: '',
        bank_contract: '',
        bank_role: '',
    };
    @observable err = this.makeErrObject();
    customerTypes = [{v: 'Бізнес'}, {v: 'Процесинг (Канали зв’язку)'}, {v: 'Діспути (Ризик-моніторинг)'}, {v: 'Розрахунки'}];
    bankRoles = [{v: 'емітент/еквайр'}, {v: 'емітент'}, {v: 'еквайр'} ];

    render() {
        return (
            <div className="page-content login-cover">
                <div className="content-wrapper">
                    <div className="content d-flex justify-content-center align-items-center">
                        <form className="registration-form" onSubmit={this.onSubmit} autoComplete="off" noValidate>
                            <div className="card mb-0">
                                <div className="card-body">

                                    <div className="text-center mb-3">
                                        <i className="icon-reading icon-2x text-slate-300 border-slate-300 border-3 rounded-round p-3 mb-3 mt-1"/>
                                        <h5 className="mb-0">Створіть свій акаунт</h5>
                                    </div>

                                    <div className="row">
                                        <div className="col-md-6">
                                            <div className="form-group">
                                                <input
                                                    type="text" className="form-control" placeholder="Ваше і’мя"
                                                    name="user_name" value={this.fd.user_name} required
                                                    onChange={this.onInputChange}
                                                    onBlur={this.validate}
                                                />
                                                {this.err.user_name &&
                                                <span className="form-text text-danger">{this.err.user_name}</span>}
                                            </div>
                                            <div className="form-group">
                                                <input
                                                    type="email" className="form-control" placeholder="Ваш e-mail"
                                                    name="user_email" value={this.fd.user_email} required
                                                    onChange={this.onInputChange}
                                                    onBlur={this.validate}
                                                />
                                                {this.err.user_email &&
                                                <span className="form-text text-danger">{this.err.user_email}</span>}
                                            </div>
                                            <div className="form-group">
                                                <input
                                                    type="password" className="form-control" placeholder="Пароль"
                                                    name="user_password" value={this.fd.user_password}
                                                    onChange={this.onInputChange}
                                                    onBlur={this.validate}
                                                />
                                                {this.err.user_password &&
                                                <span className="form-text text-danger">{this.err.user_password}</span>}
                                            </div>
                                            <div className="form-group">
                                                <input
                                                    type="text" className="form-control" placeholder="Телефон користувача"
                                                    name="user_phone" value={this.fd.user_phone} required
                                                    onChange={this.onInputChange}
                                                    onBlur={this.validate}
                                                />
                                                {this.err.user_phone &&
                                                <span className="form-text text-danger">{this.err.user_phone}</span>}
                                            </div>
                                            <div className="form-group">
                                                <input
                                                    type="text" className="form-control" placeholder="Посада"
                                                    name="user_position" value={this.fd.user_position} required
                                                    onChange={this.onInputChange}
                                                    onBlur={this.validate}
                                                />
                                                {this.err.user_position &&
                                                <span className="form-text text-danger">{this.err.user_position}</span>}
                                            </div>
                                            <div className="form-group">
                                                <select
                                                    className="form-control"
                                                    name="bank_user_type" value={this.fd.bank_user_type} required
                                                    onChange={this.onInputChange}
                                                    onBlur={this.validate}
                                                >
                                                    <option value="" disabled>Тип коричтувача</option>
                                                    {this.customerTypes.map(x => (
                                                        <option key={x.v} value={x.v}>{x.v}</option>
                                                    ))}
                                                </select>
                                                {this.err.bank_user_type &&
                                                <span className="form-text text-danger">{this.err.bank_user_type}</span>}
                                            </div>
                                        </div>

                                        <div className="col-md-6">
                                            <div className="form-group">
                                                <input
                                                    type="text" className="form-control" placeholder="Назва банку"
                                                    name="bank_name" value={this.fd.bank_name} required
                                                    onChange={this.onInputChange}
                                                    onBlur={this.validate}
                                                />
                                                {this.err.bank_name &&
                                                <span className="form-text text-danger">{this.err.bank_name}</span>}
                                            </div>
                                            <div className="form-group">
                                                <input
                                                    type="text" className="form-control" placeholder="Адреса банку"
                                                    name="bank_address" value={this.fd.bank_address} required
                                                    onChange={this.onInputChange}
                                                    onBlur={this.validate}
                                                />
                                                {this.err.bank_address &&
                                                <span className="form-text text-danger">{this.err.bank_address}</span>}
                                            </div>
                                            <div className="form-group">
                                                <input
                                                    type="text" className="form-control" placeholder="Телефони банку"
                                                    name="bank_phones" value={this.fd.bank_phones} required
                                                    onChange={this.onInputChange}
                                                    onBlur={this.validate}
                                                />
                                                {this.err.bank_phones &&
                                                <span className="form-text text-danger">{this.err.bank_phones}</span>}
                                            </div>
                                            <div className="form-group">
                                                <input
                                                    type="text" className="form-control" placeholder="Emails банку"
                                                    name="bank_emails" value={this.fd.bank_emails} required
                                                    onChange={this.onInputChange}
                                                    onBlur={this.validate}
                                                />
                                                {this.err.bank_emails &&
                                                <span className="form-text text-danger">{this.err.bank_emails}</span>}
                                            </div>
                                            <div className="form-group">
                                                <input
                                                    type="text" className="form-control" placeholder="Ідентифікаційний номер емітента"
                                                    name="bank_ihe" value={this.fd.bank_ihe} required
                                                    onChange={this.onInputChange}
                                                    onBlur={this.validate}
                                                />
                                                {this.err.bank_ihe &&
                                                <span className="form-text text-danger">{this.err.bank_ihe}</span>}
                                            </div>
                                            <div className="form-group">
                                                <input
                                                    type="text" className="form-control" placeholder="Реквізити свідоцтва про участь банку в ПРОСТІР"
                                                    name="bank_certificate" value={this.fd.bank_certificate} required
                                                    onChange={this.onInputChange}
                                                    onBlur={this.validate}
                                                />
                                                {this.err.bank_certificate &&
                                                <span className="form-text text-danger">{this.err.bank_certificate}</span>}
                                            </div>
                                            <div className="form-group">
                                                <input
                                                    type="text" className="form-control" placeholder="Реквізити договору, укладеного з ПРОСТІР"
                                                    name="bank_contract" value={this.fd.bank_contract} required
                                                    onChange={this.onInputChange}
                                                    onBlur={this.validate}
                                                />
                                                {this.err.bank_contract &&
                                                <span className="form-text text-danger">{this.err.bank_contract}</span>}
                                            </div>
                                            <div className="form-group">
                                                <select
                                                    className="form-control"
                                                    name="bank_role" value={this.fd.bank_role} required
                                                    onChange={this.onInputChange}
                                                    onBlur={this.validate}
                                                >
                                                    <option value="" disabled>Роль в ПРОСТІР</option>
                                                    {this.bankRoles.map(x => (
                                                        <option key={x.v} value={x.v}>{x.v}</option>
                                                    ))}
                                                </select>
                                                {this.err.bank_role &&
                                                <span className="form-text text-danger">{this.err.bank_role}</span>}
                                            </div>
                                        </div>
                                    </div>

                                    <div className="form-group mt-3 d-flex justify-content-between">
                                        <Link to="/login">Авторизуватися</Link>
                                        <button type="submit" className="btn btn-primary">
                                            Реєстрація <i className="icon-circle-right2 ml-2"/>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        );
    }

    submit() {
        registration(this.fd)
            .then(action(res => {
                if (res.errors) for (let k in res.errors) this.err[k] = res.errors[k];

                if(!res.details) this.props.history.push('/success-registration');
            }))
    };
}

export default Registration;
