import React from 'react';
import {action, observable} from 'mobx';
import {observer} from 'mobx-react';
import {Link} from 'react-router-dom';
import FormComponent from '../../abstract/FormComponent';
import {login} from '../../api';

@observer
class LoginPage extends FormComponent {
    @observable fd = {email: '', password: '', remember: true};
    @observable err = this.makeErrObject();

    render() {
        return (
            <div className="page-content login-cover">
                <div className="content-wrapper">
                    <div className="content d-flex justify-content-center align-items-center">
                        <form className="login-form" onSubmit={this.onSubmit} autoComplete="off" noValidate>
                            <div className="card mb-0">
                                <div className="card-body">

                                    <div className="text-center mb-3">
                                        <i className="icon-reading icon-2x text-slate-300 border-slate-300 border-3 rounded-round p-3 mb-3 mt-1"/>
                                        <h5 className="mb-0">Увійти в свій акаунт</h5>
                                        <span className="d-block text-muted">Ваші дані</span>
                                    </div>

                                    <div className="form-group form-group-feedback form-group-feedback-left">
                                        <input
                                            type="email" className="form-control" placeholder="Ваш e-mail"
                                            name="email" value={this.fd.email} required
                                            onChange={this.onInputChange}
                                            onBlur={this.validate}
                                        />
                                        {this.err.email &&
                                        <span className="form-text text-danger">{this.err.email}</span>}
                                        <div className="form-control-feedback">
                                            <i className="icon-mail5 text-muted"/>
                                        </div>
                                    </div>

                                    <div className="form-group form-group-feedback form-group-feedback-left">
                                        <input
                                            type="password" className="form-control" placeholder="Пароль"
                                            name="password" value={this.fd.password} required
                                            onChange={this.onInputChange}
                                            onBlur={this.validate}
                                        />
                                        {this.err.password &&
                                        <span className="form-text text-danger">{this.err.password}</span>}
                                        <div className="form-control-feedback">
                                            <i className="icon-lock2 text-muted"/>
                                        </div>
                                    </div>

                                    <div className="form-group d-flex justify-content-between">
                                        <div className="form-check">
                                            <label className="form-check-label">
                                                <div className="uniform-checker">
                                                    <input
                                                        type="checkbox" className="form-check-input-styled"
                                                        name="remember" checked={this.fd.remember}
                                                        onChange={this.onInputChange}
                                                    />
                                                    <span/>
                                                </div>
                                                Запам'ятати
                                            </label>
                                        </div>
                                        <Link to="/registration">Реєстрація</Link>
                                    </div>

                                    <div className="form-group">
                                        <button type="submit" className="btn btn-primary btn-block">
                                            Увійти <i className="icon-circle-right2 ml-2"/>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        );
    }

    submit() {
        login(this.fd)
            .then(action(res => {
                if(res.errors) this.setErrors(res.errors);
            }))
    }
}

export default LoginPage;
