import React, {Component} from 'react';
import {action, observable} from 'mobx';
import {observer} from 'mobx-react';
import withQueryParams from 'react-router-query-params';
import SortTh from '../components/SortTh';
import Pagination from '../components/Pagination';
import ConstructedForm from './Forms/ConstructedForm';
import {dateFormat} from '../helpers';
import {getFormById, getFormsHistoryItem, getFormsHistoryList} from '../api';

@withQueryParams({
    stripUnknownKeys: true,
    keys: {
        page:  {default: '1', validate: v => v && +v > 0},
        sort:  {default: 'id', validate: v => v},
        order: {default: 'asc', validate: v => !!v && ['asc', 'desc'].includes(v)},
    }
})
@observer
class FormsHistoryList extends Component {
    @observable loading = true;
    @observable aForm = null;
    @observable data = {items: [], totalCount: 0};

    componentDidMount() {
        this.fetchData()
    }

    componentDidUpdate(prevProps) {
        if(prevProps.location.search !== this.props.location.search) {
            this.fetchData();
        }
    }

    fetchData() {
        const qp = this.props.queryParams;
        getFormsHistoryList(qp).then(action(data => {
            if(data) this.data = data;
            this.loading = false;
        }));
    }

    render() {
        if(this.loading) return <div className="content">Loading ...</div>;

        return (
            <div className="content">
                <div className="card">
                    <div className="card-header header-elements-inline">
                        <h5 className="card-title">Звіти</h5>
                    </div>
                    <table className="table datatable-selection-single">
                        <thead>
                        <tr>
                            <SortTh sort="form_name">Назва форми</SortTh>
                            <SortTh sort="create_time">Час</SortTh>
                            <th/>
                        </tr>
                        </thead>
                        <tbody>
                        {
                            this.data.items.map((item) => (
                                <tr key={item.id}>
                                    <td>{item.form_name}</td>
                                    <td>{dateFormat(item.create_time)}</td>
                                    <td className="text-right">
                                        <button
                                            className="btn btn-primary btn-sm legitRipple"
                                            onClick={() => this.openConstructedForm(item.id, item.form_id)}
                                        >Переглянути</button>
                                    </td>
                                </tr>
                            ))
                        }
                        </tbody>
                    </table>
                    <Pagination total={this.data.totalCount}/>
                </div>

                {this.aForm &&
                <ConstructedForm
                    model={this.aForm}
                    onClose={action(() => this.aForm = null)}
                />
                }
            </div>
        );
    }

    openConstructedForm(id, form_id) {
        Promise.all([
            getFormsHistoryItem(id),
            getFormById(form_id)
        ])
        .then(action(([formData, formModel]) => {
            for(let k in formModel.fields) {
                const field = formModel.fields[k];
                field.value = formData[field.name];
            }

            formModel.is_updated = formData.is_updated;
            formModel.history_id = id;

            console.log('model', formModel);
            this.aForm = formModel;
        }))
    }
}

export default FormsHistoryList;
