import React, {Component} from 'react';
import {action, observable} from 'mobx';
import {observer} from 'mobx-react';
import * as cn from 'classnames';
import {getDocumentCategoryList, getDocuments} from '../../api';

@observer
class DocumentList extends Component {
    @observable loading = true;
    @observable categories = [];
    @observable data = [];
    @observable aCategoryId = null;
    @observable aItem = null;

    componentDidMount() {
        this.fetchData();
    }

    fetchData() {
        getDocumentCategoryList().then(action(data => {
            if(data) this.categories = data;
            this.loading = false;
        }));
    }

    render() {
        if(this.loading) return <div className="content">Loading ...</div>;

        return (
            <div className="content">
                <div className="card">
                    <div className="card-header header-elements-inline">
                        <h5 className="card-title">Документи</h5>
                    </div>
                    <div className="row">
                        <div className="col-md-6">
                            <table className="table datatable-selection-single pr-choice-table">
                                <thead><tr><th>Категорія</th></tr></thead>
                                <tbody>
                                {
                                    this.categories.map((item) => (
                                        <tr
                                            key={item.id}
                                            className={cn({a: this.aCategoryId === item.id})}
                                            onClick={() => this.setCategory(item.id)}
                                        ><td>{item.category}</td></tr>
                                    ))
                                }
                                </tbody>
                            </table>
                        </div>
                        <div className="col-md-6">
                            <table className="table datatable-selection-single pr-choice-table">
                                <thead><tr><th>Назва документу</th></tr></thead>
                                <tbody>
                                {
                                    this.data.map((item) => (
                                        <tr key={item.id}>
                                            <td>
                                                <a href={item.link} target="_blank" rel="noopener noreferrer">
                                                    {item.title}
                                                </a>
                                            </td>
                                        </tr>
                                    ))
                                }
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    @action
    setCategory(id) {
        this.data = [];
        this.aCategoryId = id;
        getDocuments(id).then(action(data => { if(data) this.data = data; }));
    }
}

export default DocumentList;
