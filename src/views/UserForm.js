import React from 'react';
import {observer} from 'mobx-react';
import {action, observable} from 'mobx';
import FormInput from '../components/FormInput';
import FormHeader from '../components/FormHeader';
import FormComponent from '../abstract/FormComponent';
import ReadOnlyRowInput from '../components/ReadOnlyRowInput';
import {showMsg} from '../helpers';
import {getUser, saveBank, saveUser} from '../api';

@observer
class UserForm extends FormComponent {
    id = this.props.match.params.id;
    @observable fd = {
        password: '',
        avatar: '',
        bank_logo: ''
    };
    @observable data = {};
    @observable err = this.makeErrObject();

    componentDidMount() {
        this.fetchData();
    }

    fetchData() {
        getUser().then(action(data => {
            for(let k in this.fd) if(data[k]) this.fd[k] = data[k];
            this.fd.bank_logo = data.bank.logo;
            this.data = data;
            this.loading = false;
        }));
    }

    render() {
        if(this.loading) return <div className="content">Loading ...</div>;

        return (
            <div className="content">
                <form onSubmit={this.onSubmit} autoComplete="off" noValidate>
                    <div className="card">
                        <FormHeader>{'Редагування профілю'}</FormHeader>
                        <div className="card-body">
                            <fieldset className="col-md-8">
                                <ReadOnlyRowInput label="Email" value={this.data.email} />
                                <ReadOnlyRowInput label="Телефон" value={this.data.phone} />
                                <ReadOnlyRowInput label="Ім'я" value={this.data.name} />
                                <ReadOnlyRowInput label="Тип коричтувача" value={this.data.bank_user_type} />
                                <ReadOnlyRowInput label="Посада" value={this.data.position} />
                                <FormInput
                                    label="Password" type="password" name="password" form={this} notRequired
                                    onChange={this.onInputChange} onBlur={this.validate}
                                />
                                <div className="form-group row">
                                    <label className="col-form-label col-lg-4">Аватар</label>
                                    <div className="col-lg-8">
                                        <div className="uniform-uploader">
                                            <input
                                                type="file" className="form-input-styled"
                                                name="avatar"
                                                onChange={this.onFileInputChange} accept="image/*"
                                            />
                                            <span
                                                className="action btn bg-pink-400 legitRipple">Вибрати зображення</span>
                                            {this.fd.avatar &&
                                            <img src={this.fd.avatar} className="input-file-review" alt=""/>}
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                    </div>

                    <div className="card">
                        <FormHeader>{'Інформація банку'}</FormHeader>
                        <div className="card-body">
                            <fieldset className="col-md-8">
                                <ReadOnlyRowInput label="Назва банку" value={this.data.bank.name} />
                                <ReadOnlyRowInput label="Адреса банку" value={this.data.bank.address} />
                                <ReadOnlyRowInput label="Телефони банку" value={this.data.bank.phones} />
                                <ReadOnlyRowInput label="Emails" value={this.data.bank.emails} />
                                <ReadOnlyRowInput label="Ідентифікаційний номер емітента" value={this.data.bank.ihe} />
                                <ReadOnlyRowInput label="Реквізити свідоцтва про участь банку в ПРОСТІР" value={this.data.bank.certificate} />
                                <ReadOnlyRowInput label="Реквізити договору, укладеного з ПРОСТІР" value={this.data.bank.contract} />
                                <ReadOnlyRowInput label="Роль в ПРОСТІР" value={this.data.bank.role} />
                                <div className="form-group row">
                                    <label className="col-form-label col-lg-4">Лого</label>
                                    <div className="col-lg-8">
                                        <div className="uniform-uploader">
                                            <input
                                                type="file" className="form-input-styled"
                                                name="bank_logo"
                                                onChange={this.onFileInputChange} accept="image/*"
                                            />
                                            <span
                                                className="action btn bg-pink-400 legitRipple">Вибрати зображення</span>
                                            {this.fd.bank_logo &&
                                            <img src={this.fd.bank_logo} className="input-file-review" alt=""/>}
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                    </div>

                    <div className="card">
                        <div className="card-body">
                            <div className="col-md-12 text-right">
                                <button type="submit" className="btn btn-primary">
                                    Submit <i className="icon-paperplane ml-2"/>
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        );
    }

    onSubmit = e => {
        e.preventDefault();
        this.validateForm(e.target);
        if(!this.isFormValid) return;

        Promise.all([
            saveUser({password: this.fd.password, avatar: this.fd.avatar}),
            saveBank({logo: this.fd.bank_logo})
        ])
        .then(action(([user, bank]) => {
            if(user.errors) this.setErrors(user.errors);
            if(bank.errors) this.setErrors(bank.errors);

            if(user.success && bank.success) this.props.history.push('/dashboard');
        }))
        .catch(err => showMsg('error', err.message));
    };
}

export default UserForm;
