import React, {Component} from 'react';
import {observer} from 'mobx-react';
import {action, observable} from 'mobx';
import DOMPurify from 'dompurify';
import FormHeader from '../../components/FormHeader';
import {getManualById} from '../../api';

@observer
class ManualItem extends Component {
    id = this.props.match.params.id;
    @observable loading = true;
    @observable data = {};
    @observable categories = [];

    componentDidMount() {
        this.fetchData();
    }

    fetchData() {
        getManualById(this.id).then(action(data => {
            if(data) this.data = data;
            this.loading = false;
        }));
    }

    render() {
        if(this.loading) return <div className="content">Loading ...</div>;

        return (
            <div className="content">
                <div className="card">
                    <FormHeader>Довідник #{this.data.id}</FormHeader>
                    <div className="card-body">
                        <div className="form-group row">
                            <label className="col-form-label col-lg-4">Назва</label>
                            <div className="col-lg-8">{this.data.title}</div>
                        </div>
                        <div className="form-group row">
                            <label className="col-form-label col-lg-4">Контент</label>
                            <div className="col-lg-8" >
                                <div dangerouslySetInnerHTML={{__html: DOMPurify.sanitize(this.data.content)}} />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default ManualItem;
