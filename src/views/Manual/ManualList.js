import React, {Component} from 'react';
import {action, observable} from 'mobx';
import {observer} from 'mobx-react';
import * as cn from 'classnames';
import {Link} from 'react-router-dom';
import {getManualCategoryList, getManuals} from '../../api';

@observer
class ManualList extends Component {
    @observable loading = true;
    @observable categories = [];
    @observable data = [];
    @observable aCategoryId = null;
    @observable aItem = null;

    componentDidMount() {
        this.fetchData();
    }

    fetchData() {
        getManualCategoryList().then(action(data => {
            if(data) this.categories = data;
            this.loading = false;
        }));
    }

    render() {
        if(this.loading) return <div className="content">Loading ...</div>;

        return (
            <div className="content">
                <div className="card">
                    <div className="card-header header-elements-inline">
                        <h5 className="card-title">Довідники</h5>
                    </div>
                    <div className="row">
                        <div className="col-md-6">
                            <table className="table datatable-selection-single pr-choice-table">
                                <thead><tr><th>Категорія</th></tr></thead>
                                <tbody>
                                {
                                    this.categories.map((item) => (
                                        <tr
                                            key={item.id}
                                            className={cn({a: this.aCategoryId === item.id})}
                                            onClick={() => this.setCategory(item.id)}
                                        ><td>{item.category}</td></tr>
                                    ))
                                }
                                </tbody>
                            </table>
                        </div>
                        <div className="col-md-6">
                            <table className="table datatable-selection-single pr-choice-table">
                                <thead><tr><th>Назва довідника</th></tr></thead>
                                <tbody>
                                {
                                    this.data.map((item) => (
                                        <tr key={item.id}>
                                            <td>
                                                <Link to={'/dashboard/manuals-item/' + item.id}>{item.title}</Link>
                                            </td>
                                        </tr>
                                    ))
                                }
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    @action
    setCategory(id) {
        this.data = [];
        this.aCategoryId = id;
        getManuals(id).then(action(data => { if(data) this.data = data; }));
    }
}

export default ManualList;
