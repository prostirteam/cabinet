import {observable, action} from 'mobx';

class AuthStore {
    @observable token = window.localStorage.getItem('token');
    @observable user = JSON.parse(window.localStorage.user || '{}');

    @action setUser(data, remember) {
        this.token = data.access_token;
        this.user = data.user;
        if(remember) {
            window.localStorage.setItem('token', this.token);
            window.localStorage.setItem('user', JSON.stringify(this.user));
        }
    }

    @action resetToken() {
        this.token = null;
        window.localStorage.removeItem('token');
    }
}

export default new AuthStore();
