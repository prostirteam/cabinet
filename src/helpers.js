import moment from 'moment';

export function showMsg(type, text = '') {
    new window.Noty({type, text}).show();
}

export const MIME_TYPES = 'image/*,application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document, application/vnd.ms-excel, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';

export function objectToArray(obj) {
    const a = [];
    for(let k in obj) {
        obj[k]._key = k;
        a.push(obj[k]);
    }
    return a;
}

export function dateFormat(date) {
    return moment(date * 1000).format('DD.MM.YYYY HH:mm:ss')
}
