import React, {Component} from 'react';
import {Route, Switch, Redirect, withRouter} from 'react-router-dom';
import {inject, observer} from 'mobx-react';
import LoginPage from './views/Auth/LoginPage';
import PwdRecoveryPage from './views/Auth/PwdRecoveryPage';
import SuccessRecoveryPage from './views/Auth/SuccessRecoveryPage';
import Registration from './views/Auth/Registration';
import SuccessRegistration from './views/Auth/SuccessRegistration';
import Dashboard from './views/Dashboard';

@inject('authStore')
@withRouter
@observer
class App extends Component {
    render() {
        const token = this.props.authStore.token;

        const pathname = this.props.location.pathname;
        const isGuestPages = [
            '/', '/registration','/success-registration', '/password-recovery', '/success-recovery'
        ].includes(pathname);

        if(!token && !isGuestPages)    return <Redirect to="/" />;
        else if(token && isGuestPages) return <Redirect to="/dashboard" />;

        return (
            <div id="app">
                <Switch>
                    <Route path='/' exact               component={LoginPage} />
                    <Route path='/registration'         component={Registration} />
                    <Route path='/success-registration' component={SuccessRegistration} />
                    <Route path='/password-recovery'    component={PwdRecoveryPage} />
                    <Route path='/success-recovery'     component={SuccessRecoveryPage} />
                    <Route path='/dashboard'            component={Dashboard} />
                </Switch>
            </div>
        );
    }
}

export default App;
