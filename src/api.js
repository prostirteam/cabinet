import authStore from './stores/authStore';
import {showMsg} from './helpers';

const API_ROOT = 'https://api-cabinet.prostir.tech';

export const ajax = {
    _ajax: (method, path, data) => {
        return new Promise(resolve => {
            const json  = JSON.stringify(data);
            const url   =  API_ROOT + path;
            const x     = new XMLHttpRequest();
            x.open(method, url, true);
            authStore.token && x.setRequestHeader('Access-Token', authStore.token);
            x.setRequestHeader('Content-type', 'application/json');
            x.responseType = 'json';
            x.onload  = () => resolve({status: x.status, data: x.response});
            x.send(json);
        });
    },
    get: url          => ajax._ajax('GET', url),
    post: (url, data) => ajax._ajax('POST', url, data),
    put: (url, data)  => ajax._ajax('PUT', url, data),
    del: url          => ajax._ajax('DELETE', url),
};

function formResMiddleware ({status, data: _data}, successMsg, errorMsg) {
    const data = {..._data};
    if(status < 300) {
        successMsg && showMsg('success', successMsg);
        data.success = true;

    } else if(status === 400 || status === 422) {
        errorMsg && showMsg('error', errorMsg);
        const errors = data.details;
        data.errors = {};
        for(let k in errors) data.errors[k] = errors[k][0];
        data.success = false;

    } else if(data && data.code === 0) {
        showMsg('error', data.details);

    } else {
        errorMsg && showMsg('error', errorMsg);
    }

    return data;
}

export function fileUpload(file) {
    return new Promise((resolve, reject) => {
        const url   =  API_ROOT + '/files';
        const x     = new XMLHttpRequest();
        const fd = new FormData();
        fd.append('file', file);
        x.open('POST', url, true);
        authStore.token && x.setRequestHeader('Access-Token', authStore.token);
        x.responseType = 'json';
        x.onload  = () => {
            if(x.status !== 200) {
                showMsg('error', x.response.details);
                reject();
            }
            resolve(x.response)
        };
        x.send(fd);
    });
}


/**  ___ Auth ___ */
export function login(data) {
    return ajax.post('/auth', data)
        .then(res => formResMiddleware(res, null, 'Login failed'))
        .then(res => {
            if(res.access_token) authStore.setUser(res, data.remember);
            return res;
        });
}
export function logout() {
    return ajax.del('/auth')
        .then(({status, data}) => {
            if(status !== 204) showMsg('error', 'Logout failed');
            authStore.resetToken();
            return data;
        });
}
export function passwordRecovery(data) {
    return new Promise(resolve => {
        setTimeout(() => {
            let res = {data: {}};

            if(data.email !== '777vbars@gmail.com')
                res = {
                    status: 400,
                    data: { details: {"email":["This email is not registered"]}}
                };

            else
                res = {status: 200, data: { }};

            resolve(res);
        }, 300);
    }).then(res => formResMiddleware(res, null, 'Password Recovery failed'));
}
export function registration(data) {
    return ajax.post('/client-registry', data)
        .then(res => formResMiddleware(res, null, 'registration failed'))
}


/**  ___ User ___ */
export function getUser() {
    return ajax.get('/users/0?expand=bank.city')
        .then(({status, data}) => {
            if(status === 200) return data;
            showMsg('error', 'Fetch User failed');
            return {};
        });
}
export function saveUser(_data) {
    const data = {..._data};
    const url = '/users/0';

    if(!data.password) delete data.password;
    if(!data.avatar)   delete data.avatar;

    return ajax.put(url, data)
        .then(res =>
            formResMiddleware(res, 'Користувач успішно збережений', 'Не вдалось зберегти користувача')
        );
}


/**  ___ Bank ___ */
export function saveBank(_data) {
    const data = {..._data};
    const url = '/banks/0';

    if(!data.logo) delete data.logo;

    return ajax.put(url, data)
        .then(res =>
            formResMiddleware(res, 'Банк успішно збережений', 'Не вдалось зберегти інформацію')
        );
}


/**  ___ Forms ___ */
export function getFormCategories() {
    return ajax.get('/form-categories?per-page=10000&sort=category')
        .then(({status, data}) => {
            if(status === 200) return data;
            showMsg('error', 'Get FormCategories failed');
        });
}
export function getForms(categoryId = null) {
    let url = '/forms?per-page=10000&sort=name';
    if(categoryId) url += `&filter[category_id]=${categoryId}`;

    return ajax.get(url)
        .then(({status, data}) => {
            if(status === 200) return data;
            showMsg('error', 'Get Forms failed');
        });
}
export function getFormById(id) {
    return ajax.get('/forms/' + id)
        .then(({status, data}) => {
            if(status === 200) return data;
            showMsg('error', 'Fetch Form failed');
            return {};
        });
}
export function sendConstructedForm(_data) {
    const data = {fields: _data.fields};
    let method = null;
    let url    = null;

    if(_data.history_id) {
        url = '/form-history/'+ _data.history_id;
        method = 'put';

    } else {
        url = '/form-history';
        method = 'post';
        data.form_id = _data.form_id;
    }

    return ajax[method](url, data)
        .then(res =>
            formResMiddleware(res, 'Форма успішно збережена.', 'Не вдалось зберегти')
        );
}


/**  ___ FormsHistoryList ___ */
export function getFormsHistoryList({sort = 'create_time', order = 'desc'} = {}) {
    const o = order === 'desc' ? '-' : '';
    return ajax.get(`/form-history?view=items&per-page=10000&sort=${o + sort}`)
        .then(({status, data}) => {
            if(status === 200) {
                const data_ = {};
                data_.items = data.items;
                data_.totalCount = data._meta.totalCount;
                return data_;
            }
            showMsg('error', 'Get FormsHistoryList failed');
        });
}
export function getFormsHistoryItem(id) {
    return ajax.get('/form-history/' + id)
        .then(({status, data}) => {
            if(status === 200) return data;
            showMsg('error', 'Fetch FormsHistoryItem failed');
            return {};
        });
}


/**  ___ Manuals ___ */
export function getManualCategoryList() {
    return ajax.get('/manual-categories?per-page=10000&sort=category')
        .then(({status, data}) => {
            if(status === 200) return data;
            showMsg('error', 'Get Manuals categories failed');
        });
}
export function getManuals(categoryId = null) {
    let url = '/manuals?per-page=10000';
    if(categoryId) url += `&filter[category_id]=${categoryId}`;

    return ajax.get(url)
        .then(({status, data}) => {
            if(status === 200) return data;
            showMsg('error', 'Get Manuals failed');
        });
}
export function getManualById(id) {
    return ajax.get('/manuals/' + id)
        .then(({status, data}) => {
            if(status === 200) return data;
            showMsg('error', 'Fetch Manual failed');
            return {};
        });
}


/**  ___ Documents ___ */
export function getDocumentCategoryList() {
    return ajax.get('/document-categories?per-page=10000&sort=category')
        .then(({status, data}) => {
            if(status === 200) return data;
            showMsg('error', 'Get Documents categories failed');
        });
}
export function getDocuments(categoryId = null) {
    let url = '/documents?per-page=10000';
    if(categoryId) url += `&filter[category_id]=${categoryId}`;
    return ajax.get(url)
        .then(({status, data}) => {
            if(status === 200) return data;
            showMsg('error', 'Get Documents failed');
        });
}
