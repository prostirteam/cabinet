import {Component} from 'react';
import {observable, action} from 'mobx';
import {observer} from 'mobx-react';
import {fileUpload} from '../api';

@observer
class FormComponent extends Component {
    @observable loading = true;
    @observable isFormValid = false;
    @observable err = {};

    makeErrObject() {
        const o = {...this.fd};
        for(let k in o) o[k] = null;
        return o;
    }

    @action
    onInputChange = e => {
        const input = e.target;
        this.fd[input.name] = input.type === 'checkbox' ? input.checked : input.value;
    };

    onFileInputChange = e => {
        const file = e.target.files[0];
        const name = e.target.name;
        fileUpload(file).then(action(res => {
            this.fd[name] = res.file;
            this.err[name] = null;
        }));
    };

    @action
    validate = e => {
        const input = e.target;
        if(!input) return console.error('input exist', e);
        this.err[input.name] = this.inputValidate(input);
        this.isFormValid = !this.hasFormErrors;
    };

    get hasFormErrors() {
        for(let k in this.err) if(this.err[k]) return true;
        return false;
    }

    validateForm(form, skip = []) {
        for(let k in this.fd) {
            if(!skip.includes(k)) this.validate({target: form.elements[k]});
        }
    }

    onSubmit = e => {
        e.preventDefault();
        this.validateForm(e.target, ['id']);
        if(!this.isFormValid) return;
        this.submit();
        console.log({...this.fd});
    };

    submit() {
    }

    @action
    setErrors(errors) {
        for(let k in this.err) if(errors[k]) this.err[k] = errors[k];
    }

    inputValidate({value, type, required, max, min, pattern}) {
        max = +max;
        min = +min;

        if(required && !value)
            return 'Поле не може бути порожнім';

        if(max && value.length > max)
            return 'Максимум - ' + max;

        if(min && value.length < min)
            return 'Мінімум - ' + min;

        if(type === 'email' && !/^[\w\d.]+@[a-zA-Z_\d]+?\.[a-zA-Z]{2,10}$/.test(value))
            return 'Невалідний Email';

        if(pattern === 'code' && !/^[\w\d\-_]+$/.test(value))
            return 'Поле повинно містити тільки латинські літери та цифры';

        if(pattern === 'phone' && !/^\+380\d+$/.test(value))
            return 'Невірний формат телефону. Потрібно +380671112233';

        return null;
    }
}

export default FormComponent;
