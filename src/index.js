import React from 'react';
import ReactDOM from 'react-dom';
import { Router } from 'react-router-dom';

import { useStrict } from 'mobx';
import { Provider } from 'mobx-react';

import createBrowserHistory from 'history/createBrowserHistory';
import promiseFinally from 'promise.prototype.finally';
import * as serviceWorker from './serviceWorker';

import App from './App';
import stores from './stores';
import './scss/index.scss';

// window._____APP_STATE_____ = stores; // For easier debugging

promiseFinally.shim();
useStrict(true);

const history = createBrowserHistory();

ReactDOM.render((
    <Provider {...stores}>
        <Router history={history}>
            <App />
        </Router>
    </Provider>

), document.getElementById('root'));

serviceWorker.unregister();
